import React from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Sidebar from "./components/Sidebar";
import Main from "./components/Main"
import {BrowserRouter} from "react-router-dom";
class App extends React.Component {
    render() {
        return (
          <BrowserRouter>
            <div class="app">
              <Header />
              <Sidebar />
              <Main />
              <Footer />
            </div>
          </BrowserRouter>
        );
    }
}
export default App;
