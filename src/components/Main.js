import React from "react";
import First from "./content/First";
import Second from "./content/Second";
import T3 from "./content/T3";
import T4 from "./content/T4";
import T5 from "./content/T5";
import T6 from "./content/T6";
import T7 from "./content/T7";
import T8 from "./content/T8";
import T9 from "./content/T9";
import T10 from "./content/T10";
import T11 from "./content/T11";
import T12 from "./content/T12";
import T13 from "./content/T13";
import T14 from "./content/T14";
import T15 from "./content/T15";
import T16 from "./content/T16";
import T17 from "./content/T17";
import T18 from "./content/T18";
import T19 from "./content/T19";
import T20 from "./content/T20";
import T21 from "./content/T21";
import T22 from "./content/T22";
import T23 from "./content/T23";
import T24 from "./content/T24";
import T25 from "./content/T25";
import T26 from "./content/T26";
import T27 from "./content/T27";
import T28 from "./content/T28";
import T29 from "./content/T29";
import T30 from "./content/T30";
import T31 from "./content/T31";
import T32 from "./content/T32";
import T33 from "./content/T33";
import T34 from "./content/T34";
import T35 from "./content/T35";
import T36 from "./content/T36";
import T37 from "./content/T37";
import T38 from "./content/T38";
import T39 from "./content/T39";
import {Route} from "react-router-dom";

class Main extends React.Component {
    render() {
        return (
          <main>
            <Route path="/content/First" component={First} />
            <Route path="/content/Second" component={Second} />
            <Route path="/content/T3" component={T3} />
            <Route path="/content/T4" component={T4} />
            <Route path="/content/T5" component={T5} />
            <Route path="/content/T6" component={T6} />
            <Route path="/content/T7" component={T7} />
            <Route path="/content/T8" component={T8} />
            <Route path="/content/T9" component={T9} />
            <Route path="/content/T10" component={T10} />
            <Route path="/content/T11" component={T11} />
            <Route path="/content/T12" component={T12} />
            <Route path="/content/T13" component={T13} />
            <Route path="/content/T14" component={T14} />
            <Route path="/content/T15" component={T15} />
            <Route path="/content/T16" component={T16} />
            <Route path="/content/T17" component={T17} />
            <Route path="/content/T18" component={T18} />
            <Route path="/content/T19" component={T19} />
            <Route path="/content/T20" component={T20} />
            <Route path="/content/T21" component={T21} />
            <Route path="/content/T22" component={T22} />
            <Route path="/content/T23" component={T23} />
            <Route path="/content/T24" component={T24} />
            <Route path="/content/T25" component={T25} />
            <Route path="/content/T26" component={T26} />
            <Route path="/content/T27" component={T27} />
            <Route path="/content/T28" component={T28} />
            <Route path="/content/T29" component={T29} />
            <Route path="/content/T30" component={T30} />
            <Route path="/content/T31" component={T31} />
            <Route path="/content/T32" component={T32} />
            <Route path="/content/T33" component={T33} />
            <Route path="/content/T34" component={T34} />
            <Route path="/content/T35" component={T35} />
            <Route path="/content/T36" component={T36} />
            <Route path="/content/T37" component={T37} />
            <Route path="/content/T38" component={T38} />
            <Route path="/content/T39" component={T39} />
          </main>
        )
    }
}

export default Main;
