import React from "react";
import {Link} from "react-router-dom";

class Sidebar extends React.Component {
    render() {
        return (
          <div class="sidebar">
            <div class="sections">
              <details>
                <summary><span>Введение</span></summary>
                <div class="topics">
                  <Link to="/content/First">Область применения компьютерных сетей</Link>
                  <Link to="/content/Second">История развития компьютерных сетей</Link>
                  <Link to="/content/T3">Понятие компьютерной сети</Link>
                  <Link to="/content/T4">Состав компьютерной сети</Link>
                  <Link to="/content/T5">Основные аппаратные и программные компоненты сети</Link>
                  <Link to="/content/T6">Развитие современных вычислительных сетей</Link>
                  <Link to="/content/T7">Классификация компьютерных сетей</Link>
                  <Link to="/content/T8">Классификация компьютерных сетей по методу доступа к физической среде передачи данных</Link>
                  <Link to="/content/T9">Локальные, региональные, глобальные типы сетей. Классификация по размеру</Link>
                  <Link to="/content/T10">Топология сети</Link>
                </div>
              </details>
              <details>
                <summary><span>Модель OSI</span></summary>
                <div class="topics">
                  <Link to="/content/T11">Многоуровневый подход. Протокол. Интерфейс</Link>
                  <Link to="/content/T12">Понятие сетевой модели. Основные сетевые модели</Link>
                  <Link to="/content/T13">Понятие открытой системы</Link>
                  <Link to="/content/T14">Физический уровень</Link>
                  <Link to="/content/T15">Канальный уровень</Link>
                  <Link to="/content/T16">Сетевой уровень</Link>
                  <Link to="/content/T17">Транспортный уровень</Link>
                  <Link to="/content/T18">Сеансовый уровень</Link>
                  <Link to="/content/T19">Уровень представления</Link>
                  <Link to="/content/T20">Уровень приложений</Link>
                </div>
              </details>
              <details>
                <summary><span>Сетевые протоколы</span></summary>
                <div class="topics">
                  <Link to="/content/T28">Модульность и стандартизация</Link>
                  <Link to="/content/T29">Понятие стека протоколов</Link>
                  <Link to="/content/T30">Стеки OSI, TCP/IP, IPX/SPX, NetBIOS/SMB</Link>
                  <Link to="/content/T31">Соответствие популярных стеков протоколов модели OSI</Link>
                  <Link to="/content/T37">Стек протоколов ТСР/IP, соответствие модели взаимодействия открытых систем</Link>
                </div>
              </details>
              <details>
                <summary><span>Физическая среда</span></summary>
                <div class="topics">
                  <Link to="/content/T21">Понятие, типы и аппаратура линий связи</Link>
                  <Link to="/content/T22">Характеристики линий связи</Link>
                  <Link to="/content/T23">Беспроводная и спутниковая связь. Используемые диапазоны. Частоты, используемые спутниковыми системами</Link>
                  <Link to="/content/T24">Кабели на основе неэкранированной и экранированной витой пары</Link>
                  <Link to="/content/T25">Коаксиальный кабель</Link>
                  <Link to="/content/T26">Оптоволоконный кабель</Link>
                  <Link to="/content/T27">Сравнительные характеристики кабелей</Link>
                </div>
              </details>
              <details>
                <summary><span>Технология Ethernet</span></summary>
                <div class="topics">
                  <Link to="/content/T32">Особенноcти технологии Ethernet</Link>
                  <Link to="/content/T33">Спецификации физической среды Ethernet</Link>
                  <Link to="/content/T34">Построение Ethernet на коаксиальном кабеле (толстом и тонком). Использование трансиверов, повторителей</Link>
                  <Link to="/content/T35">Построение Ethernet на основе неэкранированной витой пары. Оптоволоконный Ethernet</Link>
                  <Link to="/content/T36">Методика расчета конфигурации сети Ethernet</Link>
                  <Link to="/content/T38">Маршрутизатор</Link>
                  <Link to="/content/T39">Коммутатор</Link>
                </div>
              </details>
            </div>

          </div>
        )
    }
}

export default Sidebar;
