import React from "react";

class T19 extends React.Component {
    render() {
        return (
          <div class="content">
              <h1>Уровень представления</h1>
              <div class="line"></div>
              <p>Уровень представления (Presentation layer) отвечает за то, чтобы информация,
              посылаемая уровнем приложений одной системы, могла быть прочитана уровнем
              приложений другой системы. При необходимости уровень представлений
              преобразует форматы данных путем использования общего формата представления
              информации. </p>
              <p>Также он может выполнять сжатие (распаковку) данных с целью повышения
              пропускной способности сети. Помимо этого, на уровне представлений может
              выполняться шифрование (дешифрование) данных, например, с использованием <strong><em>протокола</em></strong> SSL (Secure Sockets Layer). </p>
              <p><strong>Задача уровня представления</strong> — согласовывает представление (синтаксис) данных
              при взаимодействии двух прикладных процессов.</p>
              <p>Функции уровня представления: </p>
              <ul>
                  <li>преобразование данных из внешнего формата во внутренний;</li>
                  <li>шифрование и расшифровка данных. </li>
              </ul>
          </div>
        )
    }
}

export default T19;
