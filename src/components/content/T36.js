import React from "react";
import tab1 from "./img/таблица.PNG";
import tab2 from "./img/таблица1.PNG";
import example from "./img/2_011.gif";


class T36 extends React.Component {
    render() {
        return (
          <div class="content">
              <h1>Методика расчета конфигурации сети Ethernet</h1>
              <div class="line"></div>
              <p>Для того, чтобы сеть Ethernet, состоящая из сегментов различной физической природы, работала корректно, необходимо, чтобы выполнялись три основных условия:</p>
              <ul>
                <li>Количество станций в сети не превышает 1024 (с учетом ограничений для коаксиальных сегментов).</li>
                <li>Удвоенная задержка распространения сигнала (Path Delay Value, PDV) между двумя самыми удаленными друг от друга станциями сети не превышает 575 битовых интервалов.</li>
                <li>Сокращение межкадрового расстояния (Interpacket Gap Shrinkage) при прохождении последовательности кадров через все повторители не более, чем на 49 битовых интервалов (напомним, что при отправке кадров станция обеспечивает начальное межкадровое расстояние в 96 битовых интервалов).</li>
              </ul>
              <p>Соблюдение этих требований обеспечивает корректность работы сети даже в случаях, когда нарушаются простые правила конфигурирования, определяющие максимальное количество повторителей и максимальную длину сегментов каждого типа.</p>
              <p>Физический смысл ограничения задержки распространения сигнала по сети уже пояснялся - соблюдение этого требования обеспечивает своевременное обнаружение коллизий.</p>
              <p>Требование на минимальное межкадровое расстояние связано с тем, что при прохождении кадра через повторитель это расстояние уменьшается. Каждый пакет, принимаемый повторителем, ресинхронизируется для исключения дрожания сигналов, накопленного при прохождении последовательности импульсов по кабелю и через интерфейсные схемы. Процесс ресинхронизации обычно увеличивает длину преамбулы, что уменьшает межкадровый интервал. При прохождении кадров через несколько повторителей межкадровый интервал может уменьшиться настолько, что сетевым адаптерам в последнем сегменте не хватит времени на обработку предыдущего кадра, в результате чего кадр будет просто потерян. Поэтому не допускается суммарное уменьшение межкадрового интервала более чем на 49 битовых интервалов. Величину уменьшения межкадрового расстояния при переходе между соседними сегментами обычно называют в англоязычной литературе Segment Variability Value, SVV, а суммарную величину уменьшения межкадрового интервала при прохождении всех повторителей - Path Variability Value, PVV. Очевидно, что величина PVV равна сумме SVV всех сегментов, кроме последнего.</p>
              <h2>Расчет PDV</h2>
              <p>Для упрощения расчетов обычно используются справочные данные, содержащие значения задержек распространения сигналов в повторителях, приемопередатчиках и в различных физических средах. В таблице ниже приведены данные, необходимые для расчета значения PDV для всех физических стандартов сетей Ethernet, взятые из справочника Technical Reference Pocket Guide (Volume 4, Number 4) компании Bay Networks.</p>
              <img src={tab1} />
              <p>Поясним терминологию, использованную в этой таблице, на примере сети, изображенной на рисунке</p>
              <img src={tab2} />
              <p>Левым сегментом называется сегмент, в котором начинается путь сигнала от выхода передатчика (выход Tx) конечного узла. Затем сигнал проходит через промежуточные сегменты и доходит до приемника (вход Rx) наиболее удаленного узла наиболее удаленного сегмента, который называется правым. С каждым сегментом связана постоянная задержка, названная базой, которая зависит только от типа сегмента и от положения сегмента на пути сигнала (левый, промежуточный или правый). Кроме этого, с каждым сегментом связана задержка распространения сигнала вдоль кабеля сегмента, которая зависит от длины сегмента и вычисляется путем умножения времени распространения сигнала по одному метру кабеля (в битовых интервалах) на длину кабеля в метрах.</p>
              <p>Общее значение PDV равно сумме базовых и переменных задержек всех сегментов сети. Значения констант в таблице даны с учетом удвоения величины задержки при круговом обходе сети сигналом, поэтому удваивать полученную сумму не нужно.</p>
              <p>Так как левый и правый сегмент имеют различные величины базовой задержки, то в случае различных типов сегментов на удаленных краях сети необходимо выполнить расчеты дважды: один раз принять в качестве левого сегмента сегмент одного типа, а во второй раз - сегмент другого типа, а результатом считать максимальное значение PDV. В нашем примере крайние сегменты сети принадлежат к одному типу - стандарту 10Base-T, поэтому двойной расчет не требуется, но если бы они были сегментами разного типа, то в первом случае нужно было бы принять в качестве левого сегмент между станцией и концентратором 1, а во втором считать левым сегмент между станцией и концентратором 5.</p>
              <p>Рассчитаем значение PDV для нашего примера.</p>
              <p>Левый сегмент 1: 15.3 (база) + 100 м ґ 0.113 /м = 26.6</p>
              <p><strong>Промежуточный сегмент 2:</strong> 33.5 + 1000 ґ 0.1 = 133.5</p>
              <p><strong>Промежуточный сегмент 3:</strong> 24 + 500 ґ 0.1 = 74.0</p>
              <p><strong>Промежуточный сегмент 4:</strong> 24 + 500 ґ 0.1 = 74.0</p>
              <p><strong>Промежуточный сегмент 5:</strong> 24 + 600 ґ 0.1 = 84.0</p>
              <p>Правый сегмент 6: 165 + 100 ґ 0.113 = 176.3</p>
              <p>Сумма всех составляющих дает значение PDV, равное 568.4.</p>
              <p>Так как значение PDV меньше максимально допустимой величины 575, то эта сеть проходит по величине максимально возможной задержки оборота сигнала. Несмотря на то, что ее общая длина больше 2500 метров.</p>
              <h2>Расчет PVV</h2>
              <p>Для расчета PVV также можно воспользоваться табличными значениями максимальных величин уменьшения межкадрового интервала при прохождении повторителей различных физических сред (таблица взята из того же справочника, что и предыдущая).</p>
              <img src={example} />
              <p>В соответствии с этими данными рассчитаем значение PVV для нашего примера.</p>
              <p>Левый сегмент 1 10Base-T: дает сокращение в 10.5 битовых интервалов</p>
              <p><strong>Промежуточный сегмент 2 10Base-FL:</strong> 8</p>
              <p><strong>Промежуточный сегмент 3 10Base-FB:</strong> 2</p>
              <p><strong>Промежуточный сегмент 4 10Base-FB:</strong> 2</p>
              <p><strong>Промежуточный сегмент 5 10Base-FB:</strong> 2</p>
              <p>Сумма этих величин дает значение PVV, равное 24.5, что меньше предельного значения в 49 битовых интервалов.</p>
              <p>В результате, приведенная в примере сеть по всем параметрам соответствует стандартам Ethernet.</p>
              </div>
        )
    }
}

export default T36;
